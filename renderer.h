#pragma once

#include "tilemap.h"
#include "tileset.h"
#include <SDL2/SDL.h>

void blitRenderTile(Tileset *tileset, size_t tileIndex, SDL_Surface *dest,
		    uint32_t x, uint32_t y, uint8_t scale);

void blitRenderTilemap(Tilemap *tilemap, SDL_Surface *dest, uint8_t scale);
