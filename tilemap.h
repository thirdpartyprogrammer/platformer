#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "tileset.h"

typedef struct Tilemap {
	uint8_t *tokens;
	size_t tokenLimit;
	Tileset *tileset;
	uint8_t width;
	uint8_t height;
} Tilemap;

Tilemap *createTilemap(Tileset *tileset, const char *filename, uint8_t width,
		       uint8_t height);

void getTileImageFromMapCoordinates(Tilemap *tilemap, uint8_t x, uint8_t y);

uint8_t getTokenFromMapCoordinates(Tilemap *tilemap, uint8_t x, uint8_t y);

void freeTilemap(Tilemap *tilemap);
