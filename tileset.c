#include "tileset.h"
#include <SDL2/SDL.h>
#include <stdint.h>

Tileset *createTileset(const char *imageSrc, uint8_t rows, uint8_t cols, uint8_t padding)
{
    Tileset *tileset = malloc(sizeof(Tileset));

    tileset->image = SDL_LoadBMP(imageSrc);

    tileset->tiles = calloc(rows * cols, sizeof(SDL_Rect));
    tileset->tilesLength = rows * cols;

    size_t tileWidth = tileset->image->w / cols;
    size_t tileHeight = tileset->image->h / rows;

    size_t i = 0;
    for (uint8_t y = 0; y < rows; y++)
    {
        for (uint8_t x = 0; x < cols; x++)
        {
            tileset->tiles[i].x = padding + tileWidth * x;
            tileset->tiles[i].y = padding + tileHeight * y;
            tileset->tiles[i].w = tileWidth - (padding * 2);
            tileset->tiles[i].h = tileHeight - (padding * 2);
            i++;
        }
    }
    return tileset;
}

void freeTileset(Tileset *tileset)
{
    SDL_FreeSurface(tileset->image);
    free(tileset->tiles);
    free(tileset);
}
