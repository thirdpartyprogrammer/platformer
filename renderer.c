#include "renderer.h"
#include "tilemap.h"
#include "tileset.h"
#include <SDL2/SDL.h>

void blitRenderTilemap(Tilemap *tilemap, SDL_Surface *dest, uint8_t scale)
{
    uint8_t x = 0;
    uint8_t y = 0;
    for (int y = 0; y < tilemap->height; y++)
    {
        for (int x = 0; x < tilemap->width; x++)
        {
            size_t index = (y * tilemap->width) + x;
            uint8_t token = tilemap->tokens[index];

            SDL_Surface *src = tilemap->tileset->image;
            SDL_Rect srcrect = tilemap->tileset->tiles[token];
            printf("(%d, %d): %d, %d, %d, %d\n", x, y, srcrect.x, srcrect.y, srcrect.w, srcrect.h);
            SDL_Rect dstrect = {
                .x = x * srcrect.w * scale,
                .y = y * srcrect.h * scale,
                .w = srcrect.w * scale,
                .h = srcrect.h * scale,
            };
            SDL_BlitSurface(src, &srcrect, dest, &dstrect);
        }
    }
}
