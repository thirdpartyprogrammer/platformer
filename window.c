#include "window.h"
#include "sdlUtils.h"
#include <SDL2/SDL.h>
#include <stdint.h>

SDL_Window *initWindow(uint16_t width, float aspectRatio)
{
    uint16_t height = getWindowHeight(width, aspectRatio);
    SDL_Window *window = SDL_CreateWindow("Platformer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height,
                                          SDL_WINDOW_SHOWN);
    if (!window)
        exitWithSdlError();
    return window;
}

uint16_t getWindowHeight(uint16_t width, float aspectRatio)
{
    uint16_t height = width * (1 / aspectRatio);
    return height;
}
