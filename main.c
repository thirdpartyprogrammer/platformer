#include "mapreader.h"
#include "renderer.h"
#include "sdlUtils.h"
#include "tilemap.h"
#include "tileset.h"
#include "window.h"
#include <SDL2/SDL.h>
#include <stdbool.h>

int main()
{
    initSdl();

    SDL_Window *window = initWindow(800, 16.0f / 9);
    SDL_Surface *windowSurface = SDL_GetWindowSurface(window);

    Tileset *tileset = createTileset("man.bmp", 3, 4, 2);
    Tilemap *tilemap = createTilemap(tileset, "map01.map", 3, 3);

    blitRenderTilemap(tilemap, windowSurface, 1);
    SDL_UpdateWindowSurface(window);

    bool keepUpdating = true;
    while (keepUpdating)
    {
        SDL_Event e;
        SDL_PollEvent(&e);
        switch (e.type)
        {
        case SDL_QUIT:
            keepUpdating = false;
            break;
        case SDL_KEYDOWN:
            if (e.key.keysym.scancode == SDL_SCANCODE_Q)
                keepUpdating = false;
        }
        if (e.type == SDL_QUIT)
            keepUpdating = false;
    }

    freeTileset(tileset);
    freeTilemap(tilemap);
    SDL_FreeSurface(windowSurface);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
