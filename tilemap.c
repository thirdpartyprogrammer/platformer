#include "tilemap.h"
#include "mapreader.h"
#include "tileset.h"
#include <stdbool.h>
#include <stdint.h>

bool charIsDigit(char ch);

Tilemap *createTilemap(Tileset *tileset, const char *filename, uint8_t width, uint8_t height)
{
    Tilemap *tilemap = malloc(sizeof(Tilemap));

    size_t tokenLimit = width * height;

    tilemap->width = width;
    tilemap->height = height;
    tilemap->tokenLimit = width * height;

    tilemap->tileset = tileset;
    tilemap->tokens = readMapFromFile(filename, tilemap->tokenLimit);

    return tilemap;
}

void freeTilemap(Tilemap *tilemap)
{
    free(tilemap);
}
