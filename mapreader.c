#include "mapreader.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

uint8_t *readMapFromFile(const char *filename, size_t tokenLimit)
{
    MapreaderState state;

    state.filename = filename;
    state.file = fopen(filename, "r");

    state.tokens = calloc(tokenLimit, sizeof(uint8_t));
    state.tokenIndex = 0;
    state.tokenLimit = tokenLimit;

    clearTokenString(&state);

    char ch;
    do
    {
        ch = fgetc(state.file);
        if (charIsDigit(ch))
        {
            addCharToTokenString(&state, ch);
        }
        else
        {
            pushTokenStringToTokens(&state);
            clearTokenString(&state);
        }
    } while (ch != EOF);

    return state.tokens;
}

bool charIsDigit(char ch)
{
    bool moreThanMinDigit = 48 <= (uint8_t)ch;
    bool lessThanMaxDigit = 57 >= (uint8_t)ch;
    bool withinLimits = moreThanMinDigit && lessThanMaxDigit;
    return withinLimits;
}

void addCharToTokenString(MapreaderState *state, char ch)
{
    if (state->tokenStringIndex >= TOKEN_STRING_LENGTH)
    {
        printf("A token in the map file '%s' is above the token character limit (%d).\n", state->filename,
               TOKEN_STRING_LENGTH);
        exit(EXIT_FAILURE);
    }
    else
    {
        state->tokenString[state->tokenStringIndex] = ch;
        state->tokenStringIndex++;
    }
}

void pushTokenStringToTokens(MapreaderState *state)
{
    if (state->tokenStringIndex == 0)
        return;
    if (state->tokenIndex >= state->tokenLimit)
    {
        printf("The file '%s' could not be parsed because it exceeded the token limit (%lu).\n", state->filename,
               state->tokenLimit);
        exit(EXIT_FAILURE);
    }
    else
    {
        uint8_t token = atoi(state->tokenString);
        state->tokens[state->tokenIndex] = token;
        state->tokenIndex++;
    }
}

void clearTokenString(MapreaderState *state)
{
    state->tokenStringIndex = 0;
    for (size_t i = 0; i < TOKEN_STRING_LENGTH; i++)
        state->tokenString[i] = 0;
}
