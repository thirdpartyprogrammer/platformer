#pragma once

#include <SDL2/SDL.h>
#include <stdint.h>

SDL_Window *initWindow(uint16_t width, float aspectRatio);

uint16_t getWindowHeight(uint16_t width, float aspectRatio);
