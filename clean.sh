#!/bin/sh
rm -rf .cache/ \
	compile_commands.json \
	CMakeFiles/ \
	CMakeCache.txt \
	cmake_install.cmake \
	.ninja_log \
	.ninja_deps \
	build.ninja \
	platformer
