#include "sdlUtils.h"
#include <SDL2/SDL.h>
#include <stdbool.h>
#include <stdlib.h>

void initSdl(void)
{
    bool badInit = SDL_Init(SDL_INIT_EVERYTHING) < 0;
    if (badInit)
        exitWithSdlError();
}

void exitWithSdlError(void)
{
    const char *sdlError = SDL_GetError();
    printf("SDL encountered an error!\n%s", sdlError);
    exit(EXIT_FAILURE);
}
