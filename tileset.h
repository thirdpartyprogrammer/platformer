#pragma once

#include <SDL2/SDL.h>
#include <stdint.h>

typedef struct Tile {
	uint8_t x;
	uint8_t y;
} Tile;

typedef struct Tileset {
	SDL_Surface *image;
	SDL_Rect *tiles;
	size_t tilesLength;
	uint8_t tileWidth;
	uint8_t tileHeight;
} Tileset;

Tileset *createTileset(const char *imageSrc, uint8_t rows, uint8_t cols,
		       uint8_t padding);

void freeTileset(Tileset *tileset);
