#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define TOKEN_STRING_LENGTH 2

typedef struct MapreaderState {
	const char *filename;
	FILE *file;

	uint8_t *tokens;
	size_t tokenIndex;
	size_t tokenLimit;

	char tokenString[TOKEN_STRING_LENGTH];
	size_t tokenStringIndex;
} MapreaderState;

uint8_t *readMapFromFile(const char *filename, size_t tokenLimit);

bool charIsDigit(char ch);

void addCharToTokenString(MapreaderState *state, char ch);

void pushTokenStringToTokens(MapreaderState *state);

void clearTokenString(MapreaderState *state);
